import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})

export class MovieComponent implements OnInit {
  @Input() data:any;
  @Output() deleteMe = new EventEmitter<any>();

  id;
  title;
  Studio;
  weekend_income;
  showMe = true;

  constructor() { }

  ngOnInit() {
    this.id = this.data.id;
    this.title = this.data.title;
    this.Studio = this.data.Studio;
    this.weekend_income = this.data.weekend_income;
  }

  deleteRow() {
    this.showMe = false;
    this.deleteMe.emit(this);
  }
}